//
// Created by petrov on 20.05.24.
//
#include <iostream>
#include <thread_pool.hpp>


int main() {
    std::atomic<int> a(0);
    threadpool tp{};
    for(unsigned int i = 0u; i < 1024 * 1024; ++i)
        tp.push([&a]() noexcept {++a; std::this_thread::sleep_for(std::chrono::microseconds(10));});
    std::cout << a << std::endl;
    tp.wait();
    std::cout << a << std::endl;
}