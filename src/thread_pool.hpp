#pragma once

#include <functional>
#include <mutex>
#include <condition_variable>
#include <queue>
#include <thread>
#include <future>


class threadpool {
public:
    explicit threadpool(size_t threads = std::thread::hardware_concurrency()) {
        if (threads == 0) {
            throw std::runtime_error("thread pool size cannot be zero");
        }
        for (auto i = 0llu; i < threads; i++) {
            workers.emplace_back([this] { worker_main(); });
        }
    }

    threadpool(const threadpool &) = delete;

    threadpool &operator=(const threadpool &) = delete;

    template<typename Fn, typename... Args>
    decltype(auto) push(Fn &&fn, Args &&... args, bool run = true) {
        using return_type = std::invoke_result_t<Fn, Args...>;
        using pack_task = std::packaged_task<return_type()>;

        auto t = std::make_shared<pack_task>(
            std::bind(std::forward<Fn>(fn), std::forward<Args>(args)...)
        );
        auto future = t->get_future(); {
            std::lock_guard lock(mtx);
            tasks.emplace([t = std::move(t)] { (*t)(); }, run);
        }

        cv.notify_one();
        return future;
    }

    ~threadpool() noexcept {
        push([] {
        }, false);
        cv.notify_all();

        for (auto &worker: workers) {
            worker.join();
        }
    }


    void wait() {
        std::unique_lock lock{mtx};
        waitCv.wait(lock, [this] { return inProgress == 0 && tasks.empty(); });
    }

private:
    using task = std::pair<std::function<void()>, bool>;

    [[nodiscard]] inline task poll_task() noexcept {
        task t;

        std::unique_lock lock(mtx);

        cv.wait(lock, [this] { return !tasks.empty(); });

        if (!tasks.empty()) {
            t = std::move(tasks.front());
            tasks.pop();
            ++inProgress;
        }
        lock.unlock();
        return t;
    }

    void worker_main() {
        while (true) {
            auto [t, run] = poll_task();
            // The thread pool is going to shutdown
            if (!run) {
                push([] {
                }, false);
                break;
            }
            t();
            std::unique_lock lock(mtx);
            --inProgress;
            lock.unlock();

            waitCv.notify_one();
        }
    }

    std::mutex mtx;
    std::size_t inProgress{0};
    std::condition_variable cv;
    std::condition_variable waitCv;
    std::queue<task> tasks;
    std::vector<std::thread> workers;
};
